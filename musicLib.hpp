#ifndef MUSICLIB_HPP
#define MUSICLIB_HPP

#include <wx/wx.h>
#include <wx/mediactrl.h>

// nadefinovani cest ke zvukovym souborum
#define HEADSHOT "/usr/local/share/sound/headshot.wav"
#define AK47 "/usr/local/share/sound/ak47.wav"
#define AWP "/usr/local/share/sound/awp.wav"
#define SECTORCLEAR "/usr/local/share/sound/Sector_Clear.wav"
#define LETSGO "/usr/local/share/sound/OK_Lets_Go.wav"

class musicLib
{
public:
	musicLib(wxWindow *parent);
	virtual ~musicLib();
	void Play(wxString input);
	void Stop();
	void HeadShot();
	void WeaponAK47();
	void WeaponAwp();
	void LetsGo();
	void SectorClear();
	
	void OnMediaStop(wxMediaEvent& event);

private:
	wxMediaCtrl* media = NULL;
};

#endif // MUSICLIB_HPP
