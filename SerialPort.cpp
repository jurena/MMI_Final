#include "SerialPort.h"
#include <termios.h>
#include <unistd.h>
#include <string.h>
#include <fcntl.h>
#include <sys/errno.h>

SerialPort::SerialPort() {

    state = Closed;
}

SerialPort::~SerialPort() {

}

void SerialPort::Open(std::string device) {

    if(device.length() > 0 ) {

        fd = open(device.data(), O_RDWR | O_NOCTTY | O_SYNC);
        if (fd < 0)
        {
            //error_message ("error %d opening %s: %s", errno, portname, strerror (errno));
            std::cout << "Invalid file descriptor" << std::endl;
            state = Error;
        } else {

            std::cout << "File descriptor opened" << std::endl;
            state = Ready;
        }

        if(state == Ready) {

            setAttributes(fd, B115200, 0);
        }

        if(state == Ready) {

            setBlocking(fd, 0);
        }
    } else {

        state = InvalidDevice;
    }

}

void SerialPort::Close() {

    if(fd > 0 ) {
        close(fd);
    }
}

SerialPort::ConnectionState SerialPort::getConnectionState() {
    return state;
}

void SerialPort::setAttributes(int fd, int speed, int parity) {

    struct termios tty;
    memset (&tty, 0, sizeof tty);
    if (tcgetattr (fd, &tty) != 0)
    {
        //error_message ("error %d from tcgetattr", errno);
        std::cout << "error " << errno << "from tcgetattr" << std::endl;
        state = Error;
        return;
    }

    cfsetospeed (&tty, speed);
    cfsetispeed (&tty, speed);

    tty.c_cflag = (tty.c_cflag & ~CSIZE) | CS8;     // 8-bit chars
    // disable IGNBRK for mismatched speed tests; otherwise receive break
    // as \000 chars
    tty.c_iflag &= ~IGNBRK;         // disable break processing
    tty.c_lflag = 0;                // no signaling chars, no echo,
    // no canonical processing
    tty.c_oflag = 0;                // no remapping, no delays
    tty.c_cc[VMIN]  = 0;            // read doesn't block
    tty.c_cc[VTIME] = 5;            // 0.5 seconds read timeout

    tty.c_iflag &= ~(IXON | IXOFF | IXANY); // shut off xon/xoff ctrl

    tty.c_cflag |= (CLOCAL | CREAD);// ignore modem controls,
    // enable reading
    tty.c_cflag &= ~(PARENB | PARODD);      // shut off parity
    tty.c_cflag |= parity;
    tty.c_cflag &= ~CSTOPB;
    tty.c_cflag &= ~CRTSCTS;

    if (tcsetattr (fd, TCSANOW, &tty) != 0)
    {
        //error_message ("error %d from tcsetattr", errno);
        std::cout << "error " << errno << "from tcsetattr" << std::endl;
        state = Error;
        return;
    }
}

void SerialPort::setBlocking(int fd, int should_block) {

    struct termios tty;
    memset (&tty, 0, sizeof tty);
    if (tcgetattr (fd, &tty) != 0)
    {
        //error_message ("error %d from tggetattr", errno);
        state = Error;
        return;
    }

    tty.c_cc[VMIN]  = should_block ? 1 : 0;
    tty.c_cc[VTIME] = 5;            // 0.5 seconds read timeout

    if (tcsetattr (fd, TCSANOW, &tty) != 0) {

        //error_message("error %d setting term attributes", errno);
        std::cout << "error " << errno << "setting term attributes" << std::endl;
        state = Error;
        return;
    }
}

ssize_t SerialPort::Write( const char *data, unsigned int length) {
    return write(fd, data, length);
}

ssize_t SerialPort::Read(char *data, unsigned int max) {

    return read(fd, data, max);
}
