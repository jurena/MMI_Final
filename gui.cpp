///////////////////////////////////////////////////////////////////////////
// C++ code generated with wxFormBuilder (version Jun  6 2014)
// http://www.wxformbuilder.org/
//
// PLEASE DO "NOT" EDIT THIS FILE!
///////////////////////////////////////////////////////////////////////////

#include "gui.h"

///////////////////////////////////////////////////////////////////////////

MainFrameBase::MainFrameBase( wxWindow* parent, wxWindowID id, const wxString& title, const wxPoint& pos, const wxSize& size, long style ) : wxFrame( parent, id, title, pos, size, style )
{
	this->SetSizeHints( wxDefaultSize, wxDefaultSize );
	
	mainSizer = new wxBoxSizer( wxVERTICAL );
	
	bSizer3 = new wxBoxSizer( wxHORIZONTAL );
	
	m_cameraPanel = new wxPanel( this, wxID_ANY, wxDefaultPosition, wxSize( -1,-1 ), wxTAB_TRAVERSAL );
	bSizer3->Add( m_cameraPanel, 1, wxEXPAND, 5 );
	
	m_panelTreshold = new wxPanel( this, wxID_ANY, wxDefaultPosition, wxSize( -1,-1 ), wxTAB_TRAVERSAL );
	bSizer3->Add( m_panelTreshold, 1, wxEXPAND, 5 );
	
	bSizer4 = new wxBoxSizer( wxVERTICAL );
	
	wxBoxSizer* bSizer2;
	bSizer2 = new wxBoxSizer( wxHORIZONTAL );
	
	m_startCam = new wxButton( this, wxID_ANY, _("Start"), wxDefaultPosition, wxDefaultSize, 0 );
	bSizer2->Add( m_startCam, 1, wxEXPAND, 5 );
	
	m_stopCam = new wxButton( this, wxID_ANY, _("Stop"), wxDefaultPosition, wxDefaultSize, 0 );
	bSizer2->Add( m_stopCam, 1, wxEXPAND, 5 );
	
	
	bSizer4->Add( bSizer2, 0, wxALIGN_CENTER_HORIZONTAL, 5 );
	
	bSizer5 = new wxBoxSizer( wxVERTICAL );
	
	wxString m_enemyChoices[] = { _("Green"), _("Blue"), _("Custom") };
	int m_enemyNChoices = sizeof( m_enemyChoices ) / sizeof( wxString );
	m_enemy = new wxRadioBox( this, wxID_ANY, _("Enemy"), wxDefaultPosition, wxDefaultSize, m_enemyNChoices, m_enemyChoices, 3, wxRA_SPECIFY_COLS );
	m_enemy->SetSelection( 0 );
	bSizer5->Add( m_enemy, 0, wxALL|wxALIGN_RIGHT, 5 );
	
	wxFlexGridSizer* fgSizer1;
	fgSizer1 = new wxFlexGridSizer( 0, 2, 0, 0 );
	fgSizer1->SetFlexibleDirection( wxBOTH );
	fgSizer1->SetNonFlexibleGrowMode( wxFLEX_GROWMODE_ALL );
	
	m_staticText1 = new wxStaticText( this, wxID_ANY, _("Hue min"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText1->Wrap( -1 );
	fgSizer1->Add( m_staticText1, 1, wxALL|wxALIGN_CENTER_VERTICAL, 5 );
	
	m_hmin = new wxSlider( this, hmin, 0, 0, 180, wxDefaultPosition, wxDefaultSize, wxSL_HORIZONTAL );
	fgSizer1->Add( m_hmin, 1, wxALL|wxALIGN_CENTER_VERTICAL|wxEXPAND, 5 );
	
	m_staticText2 = new wxStaticText( this, wxID_ANY, _("Hue max"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText2->Wrap( -1 );
	fgSizer1->Add( m_staticText2, 1, wxALL|wxALIGN_CENTER_VERTICAL, 5 );
	
	m_hmax = new wxSlider( this, hmax, 180, 0, 180, wxDefaultPosition, wxDefaultSize, wxSL_HORIZONTAL );
	fgSizer1->Add( m_hmax, 1, wxALL|wxEXPAND, 5 );
	
	m_staticText3 = new wxStaticText( this, wxID_ANY, _("Saturation min"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText3->Wrap( -1 );
	fgSizer1->Add( m_staticText3, 1, wxALL|wxALIGN_CENTER_VERTICAL, 5 );
	
	m_smin = new wxSlider( this, smin, 0, 0, 255, wxDefaultPosition, wxDefaultSize, wxSL_HORIZONTAL );
	fgSizer1->Add( m_smin, 1, wxALL|wxEXPAND, 5 );
	
	m_staticText4 = new wxStaticText( this, wxID_ANY, _("Saturation max"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText4->Wrap( -1 );
	fgSizer1->Add( m_staticText4, 1, wxALL|wxALIGN_CENTER_VERTICAL, 5 );
	
	m_smax = new wxSlider( this, smax, 255, 0, 255, wxDefaultPosition, wxDefaultSize, wxSL_HORIZONTAL );
	fgSizer1->Add( m_smax, 0, wxALL|wxEXPAND, 5 );
	
	m_staticText5 = new wxStaticText( this, wxID_ANY, _("Value min"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText5->Wrap( -1 );
	fgSizer1->Add( m_staticText5, 1, wxALL|wxALIGN_CENTER_VERTICAL, 5 );
	
	m_vmin = new wxSlider( this, vmin, 0, 0, 255, wxDefaultPosition, wxDefaultSize, wxSL_HORIZONTAL );
	fgSizer1->Add( m_vmin, 0, wxALL|wxEXPAND, 5 );
	
	m_staticText6 = new wxStaticText( this, wxID_ANY, _("Value max"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText6->Wrap( -1 );
	fgSizer1->Add( m_staticText6, 1, wxALL|wxALIGN_CENTER_VERTICAL, 5 );
	
	m_vmax = new wxSlider( this, vmax, 255, 0, 255, wxDefaultPosition, wxDefaultSize, wxSL_HORIZONTAL );
	fgSizer1->Add( m_vmax, 0, wxALL|wxEXPAND, 5 );
	
	m_autoscanCheck = new wxCheckBox( this, wxID_ANY, _("Autoscan"), wxDefaultPosition, wxDefaultSize, 0 );
	m_autoscanCheck->SetValue(true); 
	fgSizer1->Add( m_autoscanCheck, 0, wxALL|wxEXPAND, 5 );
	
	m_defaultPos = new wxButton( this, wxID_ANY, _("Def. pos"), wxDefaultPosition, wxDefaultSize, 0 );
	fgSizer1->Add( m_defaultPos, 0, wxALL|wxEXPAND, 5 );
	
	
	bSizer5->Add( fgSizer1, 0, wxEXPAND|wxALIGN_RIGHT, 5 );
	
	
	bSizer4->Add( bSizer5, 0, wxEXPAND|wxALIGN_RIGHT, 5 );
	
	
	bSizer3->Add( bSizer4, 0, wxEXPAND, 5 );
	
	
	mainSizer->Add( bSizer3, 0, wxEXPAND, 5 );
	
	
	this->SetSizer( mainSizer );
	this->Layout();
	mainSizer->Fit( this );
	m_statusBar2 = this->CreateStatusBar( 1, wxST_SIZEGRIP, wxID_ANY );
	
	this->Centre( wxBOTH );
	
	// Connect Events
	this->Connect( wxEVT_CLOSE_WINDOW, wxCloseEventHandler( MainFrameBase::OnCloseFrame ) );
	this->Connect( wxEVT_PAINT, wxPaintEventHandler( MainFrameBase::OnPaint ) );
	m_startCam->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( MainFrameBase::OnStartClick ), NULL, this );
	m_startCam->Connect( wxEVT_UPDATE_UI, wxUpdateUIEventHandler( MainFrameBase::OnUpdateStart ), NULL, this );
	m_stopCam->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( MainFrameBase::OnStopClick ), NULL, this );
	m_stopCam->Connect( wxEVT_UPDATE_UI, wxUpdateUIEventHandler( MainFrameBase::OnUpdateStop ), NULL, this );
	m_enemy->Connect( wxEVT_COMMAND_RADIOBOX_SELECTED, wxCommandEventHandler( MainFrameBase::OnEnemyChange ), NULL, this );
	m_hmin->Connect( wxEVT_SCROLL_TOP, wxScrollEventHandler( MainFrameBase::OnSliderScroll ), NULL, this );
	m_hmin->Connect( wxEVT_SCROLL_BOTTOM, wxScrollEventHandler( MainFrameBase::OnSliderScroll ), NULL, this );
	m_hmin->Connect( wxEVT_SCROLL_LINEUP, wxScrollEventHandler( MainFrameBase::OnSliderScroll ), NULL, this );
	m_hmin->Connect( wxEVT_SCROLL_LINEDOWN, wxScrollEventHandler( MainFrameBase::OnSliderScroll ), NULL, this );
	m_hmin->Connect( wxEVT_SCROLL_PAGEUP, wxScrollEventHandler( MainFrameBase::OnSliderScroll ), NULL, this );
	m_hmin->Connect( wxEVT_SCROLL_PAGEDOWN, wxScrollEventHandler( MainFrameBase::OnSliderScroll ), NULL, this );
	m_hmin->Connect( wxEVT_SCROLL_THUMBTRACK, wxScrollEventHandler( MainFrameBase::OnSliderScroll ), NULL, this );
	m_hmin->Connect( wxEVT_SCROLL_THUMBRELEASE, wxScrollEventHandler( MainFrameBase::OnSliderScroll ), NULL, this );
	m_hmin->Connect( wxEVT_SCROLL_CHANGED, wxScrollEventHandler( MainFrameBase::OnSliderScroll ), NULL, this );
	m_hmax->Connect( wxEVT_SCROLL_TOP, wxScrollEventHandler( MainFrameBase::OnSliderScroll ), NULL, this );
	m_hmax->Connect( wxEVT_SCROLL_BOTTOM, wxScrollEventHandler( MainFrameBase::OnSliderScroll ), NULL, this );
	m_hmax->Connect( wxEVT_SCROLL_LINEUP, wxScrollEventHandler( MainFrameBase::OnSliderScroll ), NULL, this );
	m_hmax->Connect( wxEVT_SCROLL_LINEDOWN, wxScrollEventHandler( MainFrameBase::OnSliderScroll ), NULL, this );
	m_hmax->Connect( wxEVT_SCROLL_PAGEUP, wxScrollEventHandler( MainFrameBase::OnSliderScroll ), NULL, this );
	m_hmax->Connect( wxEVT_SCROLL_PAGEDOWN, wxScrollEventHandler( MainFrameBase::OnSliderScroll ), NULL, this );
	m_hmax->Connect( wxEVT_SCROLL_THUMBTRACK, wxScrollEventHandler( MainFrameBase::OnSliderScroll ), NULL, this );
	m_hmax->Connect( wxEVT_SCROLL_THUMBRELEASE, wxScrollEventHandler( MainFrameBase::OnSliderScroll ), NULL, this );
	m_hmax->Connect( wxEVT_SCROLL_CHANGED, wxScrollEventHandler( MainFrameBase::OnSliderScroll ), NULL, this );
	m_smin->Connect( wxEVT_SCROLL_TOP, wxScrollEventHandler( MainFrameBase::OnSliderScroll ), NULL, this );
	m_smin->Connect( wxEVT_SCROLL_BOTTOM, wxScrollEventHandler( MainFrameBase::OnSliderScroll ), NULL, this );
	m_smin->Connect( wxEVT_SCROLL_LINEUP, wxScrollEventHandler( MainFrameBase::OnSliderScroll ), NULL, this );
	m_smin->Connect( wxEVT_SCROLL_LINEDOWN, wxScrollEventHandler( MainFrameBase::OnSliderScroll ), NULL, this );
	m_smin->Connect( wxEVT_SCROLL_PAGEUP, wxScrollEventHandler( MainFrameBase::OnSliderScroll ), NULL, this );
	m_smin->Connect( wxEVT_SCROLL_PAGEDOWN, wxScrollEventHandler( MainFrameBase::OnSliderScroll ), NULL, this );
	m_smin->Connect( wxEVT_SCROLL_THUMBTRACK, wxScrollEventHandler( MainFrameBase::OnSliderScroll ), NULL, this );
	m_smin->Connect( wxEVT_SCROLL_THUMBRELEASE, wxScrollEventHandler( MainFrameBase::OnSliderScroll ), NULL, this );
	m_smin->Connect( wxEVT_SCROLL_CHANGED, wxScrollEventHandler( MainFrameBase::OnSliderScroll ), NULL, this );
	m_smax->Connect( wxEVT_SCROLL_TOP, wxScrollEventHandler( MainFrameBase::OnSliderScroll ), NULL, this );
	m_smax->Connect( wxEVT_SCROLL_BOTTOM, wxScrollEventHandler( MainFrameBase::OnSliderScroll ), NULL, this );
	m_smax->Connect( wxEVT_SCROLL_LINEUP, wxScrollEventHandler( MainFrameBase::OnSliderScroll ), NULL, this );
	m_smax->Connect( wxEVT_SCROLL_LINEDOWN, wxScrollEventHandler( MainFrameBase::OnSliderScroll ), NULL, this );
	m_smax->Connect( wxEVT_SCROLL_PAGEUP, wxScrollEventHandler( MainFrameBase::OnSliderScroll ), NULL, this );
	m_smax->Connect( wxEVT_SCROLL_PAGEDOWN, wxScrollEventHandler( MainFrameBase::OnSliderScroll ), NULL, this );
	m_smax->Connect( wxEVT_SCROLL_THUMBTRACK, wxScrollEventHandler( MainFrameBase::OnSliderScroll ), NULL, this );
	m_smax->Connect( wxEVT_SCROLL_THUMBRELEASE, wxScrollEventHandler( MainFrameBase::OnSliderScroll ), NULL, this );
	m_smax->Connect( wxEVT_SCROLL_CHANGED, wxScrollEventHandler( MainFrameBase::OnSliderScroll ), NULL, this );
	m_vmin->Connect( wxEVT_SCROLL_TOP, wxScrollEventHandler( MainFrameBase::OnSliderScroll ), NULL, this );
	m_vmin->Connect( wxEVT_SCROLL_BOTTOM, wxScrollEventHandler( MainFrameBase::OnSliderScroll ), NULL, this );
	m_vmin->Connect( wxEVT_SCROLL_LINEUP, wxScrollEventHandler( MainFrameBase::OnSliderScroll ), NULL, this );
	m_vmin->Connect( wxEVT_SCROLL_LINEDOWN, wxScrollEventHandler( MainFrameBase::OnSliderScroll ), NULL, this );
	m_vmin->Connect( wxEVT_SCROLL_PAGEUP, wxScrollEventHandler( MainFrameBase::OnSliderScroll ), NULL, this );
	m_vmin->Connect( wxEVT_SCROLL_PAGEDOWN, wxScrollEventHandler( MainFrameBase::OnSliderScroll ), NULL, this );
	m_vmin->Connect( wxEVT_SCROLL_THUMBTRACK, wxScrollEventHandler( MainFrameBase::OnSliderScroll ), NULL, this );
	m_vmin->Connect( wxEVT_SCROLL_THUMBRELEASE, wxScrollEventHandler( MainFrameBase::OnSliderScroll ), NULL, this );
	m_vmin->Connect( wxEVT_SCROLL_CHANGED, wxScrollEventHandler( MainFrameBase::OnSliderScroll ), NULL, this );
	m_vmax->Connect( wxEVT_SCROLL_TOP, wxScrollEventHandler( MainFrameBase::OnSliderScroll ), NULL, this );
	m_vmax->Connect( wxEVT_SCROLL_BOTTOM, wxScrollEventHandler( MainFrameBase::OnSliderScroll ), NULL, this );
	m_vmax->Connect( wxEVT_SCROLL_LINEUP, wxScrollEventHandler( MainFrameBase::OnSliderScroll ), NULL, this );
	m_vmax->Connect( wxEVT_SCROLL_LINEDOWN, wxScrollEventHandler( MainFrameBase::OnSliderScroll ), NULL, this );
	m_vmax->Connect( wxEVT_SCROLL_PAGEUP, wxScrollEventHandler( MainFrameBase::OnSliderScroll ), NULL, this );
	m_vmax->Connect( wxEVT_SCROLL_PAGEDOWN, wxScrollEventHandler( MainFrameBase::OnSliderScroll ), NULL, this );
	m_vmax->Connect( wxEVT_SCROLL_THUMBTRACK, wxScrollEventHandler( MainFrameBase::OnSliderScroll ), NULL, this );
	m_vmax->Connect( wxEVT_SCROLL_THUMBRELEASE, wxScrollEventHandler( MainFrameBase::OnSliderScroll ), NULL, this );
	m_vmax->Connect( wxEVT_SCROLL_CHANGED, wxScrollEventHandler( MainFrameBase::OnSliderScroll ), NULL, this );
	m_autoscanCheck->Connect( wxEVT_COMMAND_CHECKBOX_CLICKED, wxCommandEventHandler( MainFrameBase::OnAutoscanChange ), NULL, this );
	m_defaultPos->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( MainFrameBase::OnDefaultClick ), NULL, this );
}

MainFrameBase::~MainFrameBase()
{
	// Disconnect Events
	this->Disconnect( wxEVT_CLOSE_WINDOW, wxCloseEventHandler( MainFrameBase::OnCloseFrame ) );
	this->Disconnect( wxEVT_PAINT, wxPaintEventHandler( MainFrameBase::OnPaint ) );
	m_startCam->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( MainFrameBase::OnStartClick ), NULL, this );
	m_startCam->Disconnect( wxEVT_UPDATE_UI, wxUpdateUIEventHandler( MainFrameBase::OnUpdateStart ), NULL, this );
	m_stopCam->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( MainFrameBase::OnStopClick ), NULL, this );
	m_stopCam->Disconnect( wxEVT_UPDATE_UI, wxUpdateUIEventHandler( MainFrameBase::OnUpdateStop ), NULL, this );
	m_enemy->Disconnect( wxEVT_COMMAND_RADIOBOX_SELECTED, wxCommandEventHandler( MainFrameBase::OnEnemyChange ), NULL, this );
	m_hmin->Disconnect( wxEVT_SCROLL_TOP, wxScrollEventHandler( MainFrameBase::OnSliderScroll ), NULL, this );
	m_hmin->Disconnect( wxEVT_SCROLL_BOTTOM, wxScrollEventHandler( MainFrameBase::OnSliderScroll ), NULL, this );
	m_hmin->Disconnect( wxEVT_SCROLL_LINEUP, wxScrollEventHandler( MainFrameBase::OnSliderScroll ), NULL, this );
	m_hmin->Disconnect( wxEVT_SCROLL_LINEDOWN, wxScrollEventHandler( MainFrameBase::OnSliderScroll ), NULL, this );
	m_hmin->Disconnect( wxEVT_SCROLL_PAGEUP, wxScrollEventHandler( MainFrameBase::OnSliderScroll ), NULL, this );
	m_hmin->Disconnect( wxEVT_SCROLL_PAGEDOWN, wxScrollEventHandler( MainFrameBase::OnSliderScroll ), NULL, this );
	m_hmin->Disconnect( wxEVT_SCROLL_THUMBTRACK, wxScrollEventHandler( MainFrameBase::OnSliderScroll ), NULL, this );
	m_hmin->Disconnect( wxEVT_SCROLL_THUMBRELEASE, wxScrollEventHandler( MainFrameBase::OnSliderScroll ), NULL, this );
	m_hmin->Disconnect( wxEVT_SCROLL_CHANGED, wxScrollEventHandler( MainFrameBase::OnSliderScroll ), NULL, this );
	m_hmax->Disconnect( wxEVT_SCROLL_TOP, wxScrollEventHandler( MainFrameBase::OnSliderScroll ), NULL, this );
	m_hmax->Disconnect( wxEVT_SCROLL_BOTTOM, wxScrollEventHandler( MainFrameBase::OnSliderScroll ), NULL, this );
	m_hmax->Disconnect( wxEVT_SCROLL_LINEUP, wxScrollEventHandler( MainFrameBase::OnSliderScroll ), NULL, this );
	m_hmax->Disconnect( wxEVT_SCROLL_LINEDOWN, wxScrollEventHandler( MainFrameBase::OnSliderScroll ), NULL, this );
	m_hmax->Disconnect( wxEVT_SCROLL_PAGEUP, wxScrollEventHandler( MainFrameBase::OnSliderScroll ), NULL, this );
	m_hmax->Disconnect( wxEVT_SCROLL_PAGEDOWN, wxScrollEventHandler( MainFrameBase::OnSliderScroll ), NULL, this );
	m_hmax->Disconnect( wxEVT_SCROLL_THUMBTRACK, wxScrollEventHandler( MainFrameBase::OnSliderScroll ), NULL, this );
	m_hmax->Disconnect( wxEVT_SCROLL_THUMBRELEASE, wxScrollEventHandler( MainFrameBase::OnSliderScroll ), NULL, this );
	m_hmax->Disconnect( wxEVT_SCROLL_CHANGED, wxScrollEventHandler( MainFrameBase::OnSliderScroll ), NULL, this );
	m_smin->Disconnect( wxEVT_SCROLL_TOP, wxScrollEventHandler( MainFrameBase::OnSliderScroll ), NULL, this );
	m_smin->Disconnect( wxEVT_SCROLL_BOTTOM, wxScrollEventHandler( MainFrameBase::OnSliderScroll ), NULL, this );
	m_smin->Disconnect( wxEVT_SCROLL_LINEUP, wxScrollEventHandler( MainFrameBase::OnSliderScroll ), NULL, this );
	m_smin->Disconnect( wxEVT_SCROLL_LINEDOWN, wxScrollEventHandler( MainFrameBase::OnSliderScroll ), NULL, this );
	m_smin->Disconnect( wxEVT_SCROLL_PAGEUP, wxScrollEventHandler( MainFrameBase::OnSliderScroll ), NULL, this );
	m_smin->Disconnect( wxEVT_SCROLL_PAGEDOWN, wxScrollEventHandler( MainFrameBase::OnSliderScroll ), NULL, this );
	m_smin->Disconnect( wxEVT_SCROLL_THUMBTRACK, wxScrollEventHandler( MainFrameBase::OnSliderScroll ), NULL, this );
	m_smin->Disconnect( wxEVT_SCROLL_THUMBRELEASE, wxScrollEventHandler( MainFrameBase::OnSliderScroll ), NULL, this );
	m_smin->Disconnect( wxEVT_SCROLL_CHANGED, wxScrollEventHandler( MainFrameBase::OnSliderScroll ), NULL, this );
	m_smax->Disconnect( wxEVT_SCROLL_TOP, wxScrollEventHandler( MainFrameBase::OnSliderScroll ), NULL, this );
	m_smax->Disconnect( wxEVT_SCROLL_BOTTOM, wxScrollEventHandler( MainFrameBase::OnSliderScroll ), NULL, this );
	m_smax->Disconnect( wxEVT_SCROLL_LINEUP, wxScrollEventHandler( MainFrameBase::OnSliderScroll ), NULL, this );
	m_smax->Disconnect( wxEVT_SCROLL_LINEDOWN, wxScrollEventHandler( MainFrameBase::OnSliderScroll ), NULL, this );
	m_smax->Disconnect( wxEVT_SCROLL_PAGEUP, wxScrollEventHandler( MainFrameBase::OnSliderScroll ), NULL, this );
	m_smax->Disconnect( wxEVT_SCROLL_PAGEDOWN, wxScrollEventHandler( MainFrameBase::OnSliderScroll ), NULL, this );
	m_smax->Disconnect( wxEVT_SCROLL_THUMBTRACK, wxScrollEventHandler( MainFrameBase::OnSliderScroll ), NULL, this );
	m_smax->Disconnect( wxEVT_SCROLL_THUMBRELEASE, wxScrollEventHandler( MainFrameBase::OnSliderScroll ), NULL, this );
	m_smax->Disconnect( wxEVT_SCROLL_CHANGED, wxScrollEventHandler( MainFrameBase::OnSliderScroll ), NULL, this );
	m_vmin->Disconnect( wxEVT_SCROLL_TOP, wxScrollEventHandler( MainFrameBase::OnSliderScroll ), NULL, this );
	m_vmin->Disconnect( wxEVT_SCROLL_BOTTOM, wxScrollEventHandler( MainFrameBase::OnSliderScroll ), NULL, this );
	m_vmin->Disconnect( wxEVT_SCROLL_LINEUP, wxScrollEventHandler( MainFrameBase::OnSliderScroll ), NULL, this );
	m_vmin->Disconnect( wxEVT_SCROLL_LINEDOWN, wxScrollEventHandler( MainFrameBase::OnSliderScroll ), NULL, this );
	m_vmin->Disconnect( wxEVT_SCROLL_PAGEUP, wxScrollEventHandler( MainFrameBase::OnSliderScroll ), NULL, this );
	m_vmin->Disconnect( wxEVT_SCROLL_PAGEDOWN, wxScrollEventHandler( MainFrameBase::OnSliderScroll ), NULL, this );
	m_vmin->Disconnect( wxEVT_SCROLL_THUMBTRACK, wxScrollEventHandler( MainFrameBase::OnSliderScroll ), NULL, this );
	m_vmin->Disconnect( wxEVT_SCROLL_THUMBRELEASE, wxScrollEventHandler( MainFrameBase::OnSliderScroll ), NULL, this );
	m_vmin->Disconnect( wxEVT_SCROLL_CHANGED, wxScrollEventHandler( MainFrameBase::OnSliderScroll ), NULL, this );
	m_vmax->Disconnect( wxEVT_SCROLL_TOP, wxScrollEventHandler( MainFrameBase::OnSliderScroll ), NULL, this );
	m_vmax->Disconnect( wxEVT_SCROLL_BOTTOM, wxScrollEventHandler( MainFrameBase::OnSliderScroll ), NULL, this );
	m_vmax->Disconnect( wxEVT_SCROLL_LINEUP, wxScrollEventHandler( MainFrameBase::OnSliderScroll ), NULL, this );
	m_vmax->Disconnect( wxEVT_SCROLL_LINEDOWN, wxScrollEventHandler( MainFrameBase::OnSliderScroll ), NULL, this );
	m_vmax->Disconnect( wxEVT_SCROLL_PAGEUP, wxScrollEventHandler( MainFrameBase::OnSliderScroll ), NULL, this );
	m_vmax->Disconnect( wxEVT_SCROLL_PAGEDOWN, wxScrollEventHandler( MainFrameBase::OnSliderScroll ), NULL, this );
	m_vmax->Disconnect( wxEVT_SCROLL_THUMBTRACK, wxScrollEventHandler( MainFrameBase::OnSliderScroll ), NULL, this );
	m_vmax->Disconnect( wxEVT_SCROLL_THUMBRELEASE, wxScrollEventHandler( MainFrameBase::OnSliderScroll ), NULL, this );
	m_vmax->Disconnect( wxEVT_SCROLL_CHANGED, wxScrollEventHandler( MainFrameBase::OnSliderScroll ), NULL, this );
	m_autoscanCheck->Disconnect( wxEVT_COMMAND_CHECKBOX_CLICKED, wxCommandEventHandler( MainFrameBase::OnAutoscanChange ), NULL, this );
	m_defaultPos->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( MainFrameBase::OnDefaultClick ), NULL, this );
	
}
