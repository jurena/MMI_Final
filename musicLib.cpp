#include "musicLib.hpp"

musicLib::musicLib(wxWindow *parent)
{
	media = new wxMediaCtrl( parent, wxID_ANY);
	media->SetVolume(1);
	//connect to the media event
	parent->Connect(wxID_ANY, wxEVT_MEDIA_STOP, (wxObjectEventFunction)(wxEventFunction)(wxMediaEventFunction) &musicLib::OnMediaStop);
}

musicLib::~musicLib()
{
	delete media;
	media = NULL;
}

void musicLib::Stop()
{
	media->Stop();
}

void musicLib::Play(wxString input)
{
	if(media->GetState() == wxMEDIASTATE_STOPPED ){
	if( media->Load(input) )
		media->Play();
	}
}

void musicLib::WeaponAK47()
{
	Play(wxString(AK47));
}

void musicLib::WeaponAwp()
{
	Play(wxString(AWP));
}

void musicLib::HeadShot()
{
	Play(wxString(HEADSHOT));
}

void musicLib::LetsGo()
{
	Play(wxString(LETSGO));
}

void musicLib::SectorClear()
{
	Play(wxString(SECTORCLEAR));
}

void musicLib::OnMediaStop(wxMediaEvent& event)
{
}
