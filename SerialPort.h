#include <bits/stringfwd.h>
#include <iostream>
#include <sys/types.h>

#ifndef SerialPort_H
#define SerialPort_H

class SerialPort
{

public:
	SerialPort();
	~SerialPort();

	void Open( std::string device );
	void Close();

	ssize_t Write( const char *data, unsigned int length );
	ssize_t Read( char * data, unsigned int max );

	enum ConnectionState {

	    Closed,
	    InvalidDevice,
	    Ready,
	    Error
	};

	SerialPort::ConnectionState getConnectionState();

private:

	void setAttributes( int fd, int speed, int parity );
	void setBlocking( int fd, int should_block );


	SerialPort::ConnectionState state;
	int fd;

};

#endif //SerialPort_H
