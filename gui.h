///////////////////////////////////////////////////////////////////////////
// C++ code generated with wxFormBuilder (version Jun  6 2014)
// http://www.wxformbuilder.org/
//
// PLEASE DO "NOT" EDIT THIS FILE!
///////////////////////////////////////////////////////////////////////////

#ifndef __GUI_H__
#define __GUI_H__

#include <wx/artprov.h>
#include <wx/xrc/xmlres.h>
#include <wx/intl.h>
#include <wx/panel.h>
#include <wx/gdicmn.h>
#include <wx/font.h>
#include <wx/colour.h>
#include <wx/settings.h>
#include <wx/string.h>
#include <wx/button.h>
#include <wx/sizer.h>
#include <wx/radiobox.h>
#include <wx/stattext.h>
#include <wx/slider.h>
#include <wx/checkbox.h>
#include <wx/statusbr.h>
#include <wx/frame.h>

///////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////
/// Class MainFrameBase
///////////////////////////////////////////////////////////////////////////////
class MainFrameBase : public wxFrame 
{
	private:
	
	protected:
		enum
		{
			hmin = 1000,
			hmax,
			smin,
			smax,
			vmin,
			vmax
		};
		
		wxBoxSizer* mainSizer;
		wxBoxSizer* bSizer3;
		wxPanel* m_cameraPanel;
		wxPanel* m_panelTreshold;
		wxBoxSizer* bSizer4;
		wxButton* m_startCam;
		wxButton* m_stopCam;
		wxBoxSizer* bSizer5;
		wxRadioBox* m_enemy;
		wxStaticText* m_staticText1;
		wxSlider* m_hmin;
		wxStaticText* m_staticText2;
		wxSlider* m_hmax;
		wxStaticText* m_staticText3;
		wxSlider* m_smin;
		wxStaticText* m_staticText4;
		wxSlider* m_smax;
		wxStaticText* m_staticText5;
		wxSlider* m_vmin;
		wxStaticText* m_staticText6;
		wxSlider* m_vmax;
		wxCheckBox* m_autoscanCheck;
		wxButton* m_defaultPos;
		wxStatusBar* m_statusBar2;
		
		// Virtual event handlers, overide them in your derived class
		virtual void OnCloseFrame( wxCloseEvent& event ) { event.Skip(); }
		virtual void OnPaint( wxPaintEvent& event ) { event.Skip(); }
		virtual void OnStartClick( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnUpdateStart( wxUpdateUIEvent& event ) { event.Skip(); }
		virtual void OnStopClick( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnUpdateStop( wxUpdateUIEvent& event ) { event.Skip(); }
		virtual void OnEnemyChange( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnSliderScroll( wxScrollEvent& event ) { event.Skip(); }
		virtual void OnAutoscanChange( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnDefaultClick( wxCommandEvent& event ) { event.Skip(); }
		
	
	public:
		
		MainFrameBase( wxWindow* parent, wxWindowID id = wxID_ANY, const wxString& title = _("BaloonShooter 0.1Alpha"), const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxSize( -1,-1 ), long style = wxCLOSE_BOX|wxDEFAULT_FRAME_STYLE|wxTAB_TRAVERSAL );
		
		~MainFrameBase();
	
};

#endif //__GUI_H__
