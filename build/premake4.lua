solution "Pavlik-Workspace"
	configurations {"Debug", "Release"}
	
	project "Pavlik"
		kind "WindowedApp"
		language "C++"
		targetdir "../bin"
		files {"../*.cpp", "../*.h"}
		
		buildoptions {"$(shell wx-config --cxxflags)", "--std=c++11"}
		linkoptions {"$(shell wx-config --libs std,media)", "`pkg-config --libs --cflags opencv`"}
		defines {"__WX__", "USE_SERIALPORT=0", "UDOO=0"}
		
		configuration "Debug"
			defines "DEBUG"
			flags {"Symbols", "ExtraWarnings"}
			linkoptions "$(shell wx-config --debug=yes --libs std,media --unicode=yes)"
			
		configuration "Release"
			defines "NDEBUG"
			flags "Optimize"
			linkoptions {"$(shell wx-config --debug=no --libs std,media --unicode=yes)", "-s"}
			
		configuration "windows"
			flags "WinMain"
			files "../*.rc"
			resoptions "$(shell wx-config --rcflags)"
