  // Sweep
  // by BARRAGAN <http://barraganstudio.com> 
  // This example code is in the public domain.
  
  #include <Servo.h> 
//  #include <FrequencyTimer2.h>


  String inputString = "";         // a string to hold incoming data
  boolean stringComplete = false;  // whether the string is complete
   
  Servo servoVertical;  // create servo object to control a servo 
  Servo servoHorizontal;  // a maximum of eight servo objects can be created 
  
  int posVertical = 90;    // variable to store the servo position 
  int posHorizontal = 90;    // variable to store the servo position 
  
  const int limVerticalMin = 30;
  const int limHorizontalMin = 30;
  const int limVerticalMax = 150;
  const int limHorizontalMax = 150;
  
  int directinoHorizontal = 1;
  int directionVertikal = 1;
  
  bool scanning = false;
   
   
  void setup() 
  { 
    servoVertical.attach(10);
    servoHorizontal.attach(9);  // attaches the servo on pin 9 to the servo object 
    Serial.begin(115200); // // opens serial port, sets data rate to 9600 bps
    inputString.reserve(200);
    
 //  FrequencyTimer2::setPeriod(400000);
 // FrequencyTimer2::enable();
 // FrequencyTimer2::setOnOverflow( scanning);

  servoVertical.write(posVertical);             
  servoHorizontal.write(posHorizontal);
  } 
  
  void Scanning()
  {
    posHorizontal = posHorizontal + directinoHorizontal;
     if(limHorizontalMin > posHorizontal)directinoHorizontal=1;
     if(limHorizontalMax < posHorizontal)directinoHorizontal=-1;
                             
      servoVertical.write(posVertical);// tell servo to go to position in variable 'pos' 
    servoHorizontal.write(posHorizontal); 
   
 
  }
 
 void SetPosition(int x, int y)
  {
    posVertical = y;
    posHorizontal = x;
     if(limVerticalMin > x)posVertical=limVerticalMin;
     if(limVerticalMax < x)posVertical=limVerticalMax;
     if(limHorizontalMin > y)posHorizontal=limHorizontalMin;
     if(limHorizontalMax < y)posHorizontal=limHorizontalMax;
                             
      servoVertical.write(posVertical);// tell servo to go to position in variable 'pos' 
    servoHorizontal.write(posHorizontal); 
   
 
  }
   
  void loop() 
  { 
    if (stringComplete) {
      
       if (inputString.substring(0,4) == "scan")
       {
          if(inputString.substring(5,7) == "on") 
         {
         posVertical = 90;
         scanning = true;
         }
         if(inputString.substring(5,8) == "off")
         scanning = false;
       }
       
       if (inputString.substring(0,6) == "setPos")
       {
         scanning = false;
         SetPosition(inputString.substring(7,10).toInt(), inputString.substring(11,14).toInt());
       }
       
       if (inputString.substring(0,4) == "move")
       {
         int x=0, y=0;
         scanning = false;
         
         if(inputString.substring(5,6) == "l")
         x= posHorizontal - inputString.substring(6,9).toInt();
         if(inputString.substring(5,6) == "r")
         x= posHorizontal + inputString.substring(6,9).toInt();
         if(inputString.substring(9,10) == "u")
         y= posVertical - inputString.substring(10,13).toInt();
         if(inputString.substring(9,10) == "d")
         y= posVertical + inputString.substring(10,13).toInt();
         
         Serial.println(inputString.substring(5,6));
         Serial.println(inputString.substring(9,10));
         Serial.println(x);
                  Serial.println(y);
         
         SetPosition(x, y);
       }
    
    Serial.println(inputString);
    inputString = "";
    stringComplete = false;
      
    }
    
    if(scanning){
    Scanning();
    delay(40);
    }
  }
  
  /* SerialEvent occurs whenever a new data comes in the
 hardware serial RX.  This routine is run between each
 time loop() runs, so using delay inside loop can delay
 response.  Multiple bytes of data may be available.
 */
 
void serialEvent() {
  while (Serial.available()) {
    // get the new byte:
    char inChar = (char)Serial.read();
    // add it to the inputString:
    inputString += inChar;
    // if the incoming character is a newline, set a flag
    // so the main loop can do something about it:
    if (inChar == '\n') {
      stringComplete = true;
    } 
  }
}
