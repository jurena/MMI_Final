#!/usr/bin/env sh

cd build

make config=release -C./premake-4.3/build/gmake.unix

./premake-4.3/bin/release/premake4 codelite
./premake-4.3/bin/release/premake4 gmake

make config=release
